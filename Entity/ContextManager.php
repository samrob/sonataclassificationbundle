<?php

namespace Sonata\ClassificationBundle\Entity;

use Sonata\ClassificationBundle\Model\Context;
use Sonata\ClassificationBundle\Model\ContextManagerInterface;
use Sonata\MediaBundle\Provider\Pool;

class ContextManager implements ContextManagerInterface
{
    /**
     * @var Pool
     */
    protected $contextsPool;

    /**
     * @var Context[]
     */
    protected $cache = [];

    /**
     * ContextManager constructor.
     * @param Pool $pool
     */
    public function __construct(Pool $pool)
    {
        $this->contextsPool = $pool;
    }

    /**
     * {@inheritdoc}
     */
    public function findAll()
    {
        $result = [];

        foreach (array_keys($this->contextsPool->getContexts()) as $context) {
            $result[] = $this->loadContext($context);
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        if (!$this->contextsPool->hasContext($id)) {
            return null;
        }

        return $this->loadContext($id);
    }

    /**
     * {@inheritdoc}
     */
    public function getDefaultContext()
    {
        return $this->loadContext($this->contextsPool->getDefaultContext());
    }

    protected function loadContext($id)
    {
        if (!isset($this->cache[$id])) {
            $this->cache[$id] = new Context($id, ucfirst($id));
        }

        return $this->cache[$id];
    }
}
