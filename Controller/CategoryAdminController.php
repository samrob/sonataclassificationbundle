<?php

namespace Sonata\ClassificationBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Category Admin Controller.
 *
 * @author Thomas Rabaix <thomas.rabaix@sonata-project.org>
 */
class CategoryAdminController extends Controller
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function listAction(Request $request = null)
    {
        if ($this->admin->isTreeMode()) {
            return $this->renderTree($request);
        }

        if ($listMode = $request->get('_list_mode')) {
            $this->admin->setListMode($listMode);
        }

        $contextManager = $this->get('sonata.classification.manager.context');
        $context = $contextManager->find($this->admin->getPersistentParameter('context'));

        $datagrid = $this->admin->getDatagrid();
        $datagrid
            ->getQuery()
            ->where('o.context = :context')
            ->setParameter(':context', $context->getId());

        $formView = $datagrid->getForm()->createView();

        $this
            ->get('twig')
            ->getExtension('form')
            ->renderer
            ->setTheme($formView, $this->admin->getFilterTheme());

        return $this->render($this->admin->getTemplate('list'), array(
            'action' => 'list',
            'form' => $formView,
            'current_context' => $context,
            'contexts' => $contextManager->findAll(),
            'datagrid' => $datagrid,
            'csrf_token' => $this->getCsrfToken('sonata.batch'),
        ));
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function renderTree(Request $request)
    {
        $contextManager = $this->get('sonata.classification.manager.context');
        $context = $contextManager->find($this->admin->getPersistentParameter('context'));

        $mainCategory = $this
            ->get('sonata.classification.manager.category')
            ->getRootCategory($context);

        $datagrid = $this->admin->getDatagrid();
        $formView = $datagrid->getForm()->createView();

        $this
            ->get('twig')
            ->getExtension('form')
            ->renderer
            ->setTheme($formView, $this->admin->getFilterTheme());

        return $this->render($this->admin->getTemplate('tree'), array(
            'action' => 'list',
            'current_context' => $context,
            'contexts' => $contextManager->findAll(),
            'main_category' => $mainCategory,
            'form' => $formView,
            'csrf_token' => $this->getCsrfToken('sonata.batch'),
        ));
    }

    protected function configure()
    {
        parent::configure();

        if ('tree' === $this->getRequest()->get('view')) {
            $this->admin->setTreeMode(true);
        }
    }
}
