<?php

namespace Sonata\ClassificationBundle\Model;

interface ContextManagerInterface
{
    /**
     * @return ContextInterface[]
     */
    public function findAll();

    /**
     * @param $id
     * @return ContextInterface|null
     */
    public function find($id);

    /**
     * @return ContextInterface
     */
    public function getDefaultContext();
}
