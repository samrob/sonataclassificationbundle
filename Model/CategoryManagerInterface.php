<?php

namespace Sonata\ClassificationBundle\Model;

use Sonata\CoreBundle\Model\ManagerInterface;
use Sonata\CoreBundle\Model\PageableManagerInterface;

interface CategoryManagerInterface extends ManagerInterface, PageableManagerInterface
{
}
