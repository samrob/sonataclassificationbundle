<?php

namespace Sonata\ClassificationBundle\Model;

interface ContextInterface
{
    /**
     * Get name.
     *
     * @return string $name
     */
    public function getName();

    /**
     * Get slug.
     *
     * @return string $slug
     */
    public function getId();
}
