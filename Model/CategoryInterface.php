<?php

namespace Sonata\ClassificationBundle\Model;

interface CategoryInterface
{
    /**
     * Set name.
     *
     * @param string $name
     */
    public function setName($name);

    /**
     * Get name.
     *
     * @return string $name
     */
    public function getName();

    /**
     * Set enabled.
     *
     * @param bool $enabled
     */
    public function setEnabled($enabled);

    /**
     * Get enabled.
     *
     * @return bool $enabled
     */
    public function getEnabled();

    /**
     * Set description.
     *
     * @param string $description
     */
    public function setDescription($description);

    /**
     * Get description.
     *
     * @return string $description
     */
    public function getDescription();

    /**
     * @param int $position
     */
    public function setPosition($position);

    /**
     * @return int
     */
    public function getPosition();

    /**
     * Add Children.
     *
     * @param CategoryInterface $children
     * @param bool              $nested
     */
    public function addChild(CategoryInterface $children, $nested = false);

    /**
     * Get Children.
     *
     * @return Collection $children
     */
    public function getChildren();

    /**
     * Set children.
     *
     * @param $children
     */
    public function setChildren($children);

    /**
     * Return true if category has children.
     *
     * @return bool
     */
    public function hasChildren();

    /**
     * Set Parent.
     *
     * @param CategoryInterface $parent
     * @param bool              $nested
     */
    public function setParent(CategoryInterface $parent = null, $nested = false);

    /**
     * Get Parent.
     *
     * @return CategoryInterface $parent
     */
    public function getParent();

    /**
     * @param ContextInterface $context
     */
    public function setContext(ContextInterface $context);

    /**
     * @return ContextInterface
     */
    public function getContext();
}
