<?php

namespace Sonata\ClassificationBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\ClassificationBundle\Model\ContextManagerInterface;
use Symfony\Component\Validator\Constraints\Valid;

class CategoryAdmin extends AbstractAdmin
{
    // NEXT_MAJOR: remove this override
    protected $formOptions = array(
        'cascade_validation' => true,
    );

    /**
     * @var ContextManagerInterface
     */
    protected $contextManager;

    /**
     * @var bool
     */
    protected $treeMode = false;

    /**
     * @param string                  $code
     * @param string                  $class
     * @param string                  $baseControllerName
     * @param ContextManagerInterface $contextManager
     */
    public function __construct($code, $class, $baseControllerName, ContextManagerInterface $contextManager)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->contextManager = $contextManager;
    }

    /**
     * {@inheritdoc}
     */
    public function getNewInstance()
    {
        $instance = parent::getNewInstance();

        if ($contextId = $this->getPersistentParameter('context')) {
            $context = $this->contextManager->find($contextId);

            if (!$context) {
                $context = $this->contextManager->getDefaultContext();
            }

            $instance->setContext($context);
        }

        return $instance;
    }

    /**
     * {@inheritdoc}
     */
    public function getPersistentParameters()
    {
        $parameters = parent::getPersistentParameters();
        $contextId = null;

        if ($this->getSubject() && $this->getSubject()->getContext()) {
            $contextId = $this->getSubject()->getContext()->getId();
        }

        if (!isset($contextId) && $this->hasRequest()) {
            $contextId = $this->getRequest()->get('context');
        }

        if (!isset($contextId) || !$this->contextManager->find($contextId)) {
            $contextId = $this->contextManager->getDefaultContext()->getId();
        }

        $parameters['context'] = $contextId;

        if ($this->isTreeMode()) {
            $parameters['view'] = 'tree';
        }

        return $parameters;
    }

    /**
     * {@inheritdoc}
     */
    public function getFormBuilder()
    {
        // NEXT_MAJOR: set constraints unconditionally
        if (isset($this->formOptions['cascade_validation'])) {
            unset($this->formOptions['cascade_validation']);
            $this->formOptions['constraints'][] = new Valid();
        } else {
            @trigger_error(<<<'EOT'
Unsetting cascade_validation is deprecated since 3.2, and will give an error in 4.0.
Override getFormBuilder() and remove the "Valid" constraint instead.
EOT
            , E_USER_DEPRECATED);
        }

        return parent::getFormBuilder();
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General', array('class' => 'col-md-6'))
                ->add('name')
                ->add('description', 'textarea', array(
                    'required' => false,
                ))
        ;

        if ($this->hasSubject()) {
            if ($this->getSubject()->getParent() !== null || $this->getSubject()->getId() === null) { // root category cannot have a parent
                $formMapper
                  ->add('parent', 'sonata_category_selector', array(
                      'category' => $this->getSubject() ?: null,
                      'model_manager' => $this->getModelManager(),
                      'class' => $this->getClass(),
                      'required' => true,
                      'context' => $this->getSubject()->getContext(),
                    ));
            }
        }

        $position = $this->hasSubject() && !is_null($this->getSubject()->getPosition()) ? $this->getSubject()->getPosition() : 0;

        $formMapper
            ->end()
            ->with('Options', array('class' => 'col-md-6'))
                ->add('enabled', null, array(
                    'required' => false,
                ))
                ->add('position', 'integer', array(
                    'required' => false,
                    'data' => $position,
                ))
            ->end()
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        if (!$this->isTreeMode()) {
            $datagridMapper
                ->add('name')
                ->add('enabled')
            ;
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('slug')
            ->add('description')
            ->add('enabled', null, array('editable' => true))
            ->add('position')
            ->add('parent', null, array(
                'sortable' => 'parent.name',
            ))
        ;
    }

    public function getListModes()
    {
        if ($this->isTreeMode()) {
            return [];
        }

        return parent::getListModes();
    }

    /**
     * @return bool
     */
    public function isTreeMode()
    {
        return $this->treeMode;
    }

    /**
     * @param bool $treeMode
     */
    public function setTreeMode(bool $treeMode)
    {
        $this->treeMode = $treeMode;
    }
}
